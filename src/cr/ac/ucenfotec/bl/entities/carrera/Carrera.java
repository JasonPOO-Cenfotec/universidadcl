package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.bl.entities.curso.Curso;

import java.util.ArrayList;

/**
 *
 */
public class Carrera {

    private String codigo;
    private String nombre;
    /**
     * Lista con los cursos que pertenecen a la carrera.
     */
    private ArrayList<Curso> cursosCarrera;
    /**
     * Booleano que determina si la carrera está acreditada por la institución contralora.
     */
    private boolean estaAcreditada;

    /**
     *
     */
    public Carrera() {
        cursosCarrera = new ArrayList<>();
    }

    /**
     *
     * @param nombre
     * @param codigo
     * @param estaAcreditada
     */
    public Carrera(String codigo, String nombre, boolean estaAcreditada) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.estaAcreditada = estaAcreditada;
        cursosCarrera = new ArrayList<>();
    }
    public Carrera(String codigo, String nombre, boolean estaAcreditada,Curso tmpCurso){
        this.codigo = codigo;
        this.nombre = nombre;
        this.estaAcreditada = estaAcreditada;
        agregarCurso(tmpCurso);
        cursosCarrera = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isEstaAcreditada() {
        return estaAcreditada;
    }

    public void setEstaAcreditada(boolean estaAcreditada) {
        this.estaAcreditada = estaAcreditada;
    }

    public void agregarCurso(Curso c){
        cursosCarrera.add(c);
   }

    public String getInfoCursos(){
        String info= "\n---- Cursos ---\n";
        for (Curso curso: cursosCarrera) {
            info += curso.toString() + "\n";
        }
        return info;
    }

    public String toString() {
        return "--- Información de la carrera ---" +
                "\ncodigo=" + codigo +
                "\nnombre=" + nombre +
                "\nacreaditada=" + estaAcreditada + getInfoCursos();
    }
}
