package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.dl.bd.Conector;
import cr.ac.ucenfotec.util.Utils;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLCarreraImpl implements ICarrera {

    private String sqlQuery = "";

    public String registrarCarrera(Carrera carrera) throws Exception {
        try {
            sqlQuery = "INSERT INTO CARRERA VALUES('"+carrera.getCodigo()+"','"+carrera.getNombre()+"')";
            Conector.getConnector().ejecutarSQL(sqlQuery);
            return "La Carrera de " + carrera.getNombre() + ", fue registrada correctamente!";

        }catch(Exception e){
            Utils.getLogger().severe("Error al registrar la carrera: "+ e.getMessage());
            return "Error grave, problemas con el registro de la carrera. Contacte al admin";
        }
    }


    public ArrayList<Carrera> listarCarreras() throws Exception {
        ArrayList<Carrera> listaCarreras = new ArrayList<>();
        try {
        sqlQuery = "SELECT CA.CODIGOCARRERA, CA.NOMBRE FROM CARRERA CA";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        while (rs.next()) {
            Carrera carrera = new Carrera(rs.getString("CODIGOCARRERA"), rs.getString("NOMBRE"), true);
            sqlQuery = "SELECT CU.CODIGOCURSO, CU.NOMBRE, CU.CREDITOS " +
                    "FROM CURSO CU JOIN CARRERA_CURSO CC " +
                    "ON CU.CODIGOCURSO = CC.CODIGOCURSO " +
                    "AND CC.CODIGOCARRERA = '" + carrera.getCodigo() + "'";

            // Consultar los cursos de esa carrera para agregarlos al objeto
            ResultSet rsCurso = Conector.getConnector().ejecutarQuery(sqlQuery);
            while(rsCurso.next()){
                 Curso curso  = new Curso(rsCurso.getString("CODIGOCURSO"),rsCurso.getString("NOMBRE"),rsCurso.getInt("CREDITOS"));
                 carrera.agregarCurso(curso);
            }

            listaCarreras.add(carrera);
        }

        return listaCarreras;

        }catch(Exception e){
            Utils.getLogger().severe("Error al listar las carreras: "+ e.getMessage());
            return new ArrayList<>();
        }

    }


    public Carrera buscarCarrera(String codigoCarrera) throws Exception {
        Carrera carrera = null;
        sqlQuery="SELECT * FROM CARRERA WHERE CODIGOCARRERA='"+codigoCarrera+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        if(rs.next()){
            carrera = new Carrera(rs.getString("CODIGOCARRERA"),rs.getString("NOMBRE"),true);
        }

        return carrera;
    }

    public String asociarCursoACarrera(Carrera carrera, Curso curso) throws Exception {
        return null;
    }
}
