package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarrera;
import cr.ac.ucenfotec.bl.entities.curso.ICurso;

public abstract class DAOFactory {

    public static final int MYSQL=1;
    public static final int MEMORY = 2;

    public static DAOFactory getDaoFactory(int repositorio){
        switch(repositorio)
        {
            case MYSQL:
                 return new MySQLDaoFactory();
            case MEMORY:
                 return new MemoryDaoFactory();
            default:
                return null;
        }
    }

    public abstract ICarrera getCarreraDao();
    public abstract ICurso getCursoDao();
}
