package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarrera;
import cr.ac.ucenfotec.bl.entities.carrera.MySQLCarreraImpl;
import cr.ac.ucenfotec.bl.entities.curso.ICurso;
import cr.ac.ucenfotec.bl.entities.curso.MySQLCursoImpl;

public class MySQLDaoFactory extends DAOFactory{

    public ICarrera getCarreraDao() {
        return new MySQLCarreraImpl();
    }

    public ICurso getCursoDao() {
        return new MySQLCursoImpl();
    }
}
