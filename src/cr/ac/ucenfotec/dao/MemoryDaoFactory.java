package cr.ac.ucenfotec.dao;

import cr.ac.ucenfotec.bl.entities.carrera.ICarrera;
import cr.ac.ucenfotec.bl.entities.carrera.MemoryCarreraImpl;
import cr.ac.ucenfotec.bl.entities.curso.ICurso;
import cr.ac.ucenfotec.bl.entities.curso.MemoryCursoImpl;

public class MemoryDaoFactory extends DAOFactory {

    public ICarrera getCarreraDao() {
        return new MemoryCarreraImpl();
    }

    public ICurso getCursoDao() {
        return new MemoryCursoImpl();
    }
}
