package cr.ac.ucenfotec.util;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Utils {

    private static Logger logger;
    private static FileHandler errorFile = null;

    public static Logger getLogger() {
        try {
            if(errorFile == null){
              logger = Logger.getLogger("Errors");
              SimpleFormatter format = new SimpleFormatter();
              errorFile = new FileHandler("LogErrores.log");
              errorFile.setFormatter(format);
              logger.setUseParentHandlers(false);
              logger.addHandler(errorFile);
            }
            return logger;
        }catch( Exception e){
            return logger;
        }
    }


    public static String[] getProperties() throws Exception{

        String[] properties = new String[6];
        Properties p = new Properties();
        String path = "src\\cr\\ac\\ucenfotec\\bd.properties";

        try {
            p.load(new FileInputStream(path));
            properties[0] = p.getProperty("driver");
            properties[1] = p.getProperty("server");
            properties[2] = p.getProperty("dataBase");
            properties[3] = p.getProperty("others");
            properties[4] = p.getProperty("user");
            properties[5] = p.getProperty("password");

            return properties;

        }catch(Exception e){
            throw e;
        }
    }
}
